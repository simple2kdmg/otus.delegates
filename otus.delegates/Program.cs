﻿using System;
using otus.delegates.Models;

namespace otus.delegates
{
    class Program
    {
        static void Main(string[] args)
        {
            var fileNames = new []{ "Passport.jpg", "Statement.txt", "Photo.jpg" };
            using var docReceiver = new DocumentReceiver(@"D:\temp", fileNames);

            docReceiver.DocumentsReady += DocumentsReadyHandler;
            docReceiver.TimedOut += TimeOutHandler;

            docReceiver.Start(20);

            Console.ReadKey();
        }

        public static void DocumentsReadyHandler(object sender, EventArgs e)
        {
            Console.WriteLine($"All documents uploaded.{Environment.NewLine}");
        }

        public static void TimeOutHandler(object sender, EventArgs e)
        {
            Console.WriteLine($"Timer expired.{Environment.NewLine}");
        }
    }
}
