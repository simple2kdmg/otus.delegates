﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Timers;

namespace otus.delegates.Models
{
    public class DocumentReceiver : IDisposable
    {
        public event EventHandler DocumentsReady;
        public event EventHandler TimedOut;

        private readonly string _path;
        private readonly IEnumerable<string> _fileNames;
        private readonly FileSystemWatcher _fsw;
        private Timer _timer;
        private Dictionary<string, bool> _documentsState;

        public DocumentReceiver(string path, IEnumerable<string> fileNames)
        {
            if (!Directory.Exists(path)) Directory.CreateDirectory(path);
            _path = path;
            _fileNames = fileNames;
            _fsw = new FileSystemWatcher(path) { EnableRaisingEvents = true };
        }

        public void Start(int seconds)
        {
            Console.WriteLine($"You have {seconds} seconds to download documents.{Environment.NewLine}");
            StartTimer(seconds);
            SubscribeOnFsw();
            UpdateDocumentsState();
        }

        private void StartTimer(int seconds)
        {
            _timer = new Timer(TimeSpan.FromSeconds(seconds).TotalMilliseconds);
            _timer.Start();
            _timer.Elapsed += OnTimerExpired;
        }

        private void UpdateDocumentsState()
        {
            _documentsState = new Dictionary<string, bool>();

            foreach (var fileName in _fileNames)
            {
                var uploaded = File.Exists($"{_path}//{fileName}");
                if (uploaded) Console.WriteLine($"Document {fileName} already uploaded.");
                _documentsState.Add(fileName, uploaded);
            }

            if (_documentsState.Values.All(uploaded => uploaded)) OnDocumentsReady();
        }

        private void SubscribeOnFsw()
        {
            _fsw.Created += OnFileChanged;
            _fsw.Renamed += OnFileChanged;
        }

        private void StopTimer()
        {
            _timer.Stop();
            _timer.Elapsed -= OnTimerExpired;
            UnsubscribeFsw();
        }

        private void OnDocumentsReady()
        {
            if (DocumentsReady != null) DocumentsReady(this, EventArgs.Empty);
            StopTimer();
        }

        private void OnTimerExpired(object sender, ElapsedEventArgs args)
        {
            if (TimedOut != null) TimedOut(this, args);
            StopTimer();
        }

        private void UnsubscribeFsw()
        {
            _fsw.Created -= OnFileChanged;
            _fsw.Renamed -= OnFileChanged;
        }

        private void OnFileChanged(object sender, FileSystemEventArgs args)
        {
            if (_documentsState.ContainsKey(args.Name))
            {
                Console.WriteLine($"{args.Name} uploaded.");
                _documentsState[args.Name] = true;
            }

            if (_documentsState.Values.All(uploaded => uploaded)) OnDocumentsReady();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
                ReleaseUnmanagedResources();
        }

        private void ReleaseUnmanagedResources()
        {
            _timer.Dispose();
            _fsw.Dispose();
        }

        ~DocumentReceiver()
        {
            Dispose(false);
        }
    }
}